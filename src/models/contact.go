package models

import (
	"github.com/jinzhu/gorm"
)

// Contact struct definition
type Contact struct {
	gorm.Model

	Username      string `gorm:"type:varchar(100);not null" json:"username"`
	ContactID     int    `gorm:"unique_index" json:"contactId"`
	ContactNumber string `gorm:"not null" json:"contactNumber"`
	ContactName   string `gorm:"not null" json:"contactName"`
}
