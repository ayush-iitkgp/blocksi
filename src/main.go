package main

import (
	"log"
	"os"
	"net/http"
	"gitlab.com/ayush-iitkgp/blocksi/src/routes"
	"github.com/gorilla/handlers"
)

func main() {
	port := os.Getenv("PORT")
	headersOK := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Access-Control-Request-Headers"})
	originsOK := handlers.AllowedOrigins([]string{"*"})
	methodsOK := handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS", "DELETE", "PUT"})

	// Handle routes
	r := routes.Handlers()
	http.Handle("/", r)
	// serve
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS(headersOK, originsOK, methodsOK)(r)))
}
