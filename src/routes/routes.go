package routes

import (
	"net/http"

	"gitlab.com/ayush-iitkgp/blocksi/src/utils/auth"

	"gitlab.com/ayush-iitkgp/blocksi/src/controllers"

	"github.com/gorilla/mux"
)

// Handlers handles routes
func Handlers() *mux.Router {

	r := mux.NewRouter().StrictSlash(true)
	r.Use(CommonMiddleware)

	r.HandleFunc("/", controllers.TestAPI).Methods("GET")
	r.HandleFunc("/register", controllers.RegisterUser).Methods("POST")
	r.HandleFunc("/login", controllers.Login).Methods("POST")

	// Auth route
	s := r.PathPrefix("/auth").Subrouter()
	s.Use(auth.JwtVerify)
	s.HandleFunc("/create/contact", controllers.CreateContact).Methods("POST")
	s.HandleFunc("/contacts", controllers.GetAllContacts).Methods("GET")
	s.HandleFunc("/contacts/{id}", controllers.GetContact).Methods("GET")
	s.HandleFunc("/contacts/{id}", controllers.UpdateContact).Methods("PUT")
	s.HandleFunc("/contacts/{id}", controllers.DeleteContact).Methods("DELETE")
	return r
}

// CommonMiddleware --Set content-type
func CommonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		next.ServeHTTP(w, r)
	})
}
