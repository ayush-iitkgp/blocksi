# JWT Authentication in Go

Created the Golang backend for login using JWT authentication with the help of the [Medium tutorial](https://medium.com/@holy_abimz/authentication-in-golang-c0677bcce1a8)

Using [Go Modules](https://go.dev/blog/using-go-modules) package manager.

## Database
> Using postgresql docker image `postgres:12` and the name of the database is `TEST` 

> Tables are `users` and `contacts`

## Running

>> Run the app on your machine
> 
>> `go run src/main.go`

>> or via docker
> 
>> `docker-compose up -d`

Your application will be accessible at [http://localhost:8080](http://localhost:8080).

Please find the example of using the API [here](https://gitlab.com/ayush-iitkgp/blocksi/-/blob/master/postman/blocksi.postman_collection.json)

Dockerized the go app using [this tutorial](https://dev.to/karanpratapsingh/dockerize-your-go-app-46pp)